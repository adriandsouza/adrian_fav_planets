import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import  axios  from "axios";
import Favourites from './components/Favourites';
import Planets from './components/Planets';
export default class App extends Component {
  state = {
    planets : [],
  }

componentDidMount(){
  axios.get(`https://assignment-machstatz.herokuapp.com/planet`).then(res=>{
    console.log(res.data);
    this.setState({planets:res.data});
  })
}


  render(){
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/favourites">
            <Favourites value={this.state.planets}/>
          </Route>
          <Route path='/'>
            <Planets planets={this.state.planets}/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
}
